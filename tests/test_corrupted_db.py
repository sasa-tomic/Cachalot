import pytest

from .context import cachalot


def corrupt_file(path):
    with open(path, 'w') as db_file:
        db_file.write(',')


@pytest.fixture(scope='session')
def cache_file(tmpdir_factory):
    return str(tmpdir_factory.mktemp('cache').join('.cache'))


@pytest.fixture
def corrupted_cache_file(tmpdir_factory):
    db_file = str(tmpdir_factory.mktemp('cache').join('.cache'))
    corrupt_file(db_file)
    return db_file


@pytest.fixture()
def corrupted_cache(cache_file):
    cache = cachalot.Cache(path=cache_file)
    corrupt_file(cache_file)
    yield cache
    cache.db.purge()


class TestCorruptedCache:

    def test_clear(self, corrupted_cache):
        # GIVEN a corrupted cache
        # WHEN clearing the cache
        corrupted_cache.clear()

        # THEN it should make an empty cache
        assert len(corrupted_cache.db) == 0

    def test_insert(self, corrupted_cache):
        # GIVEN a corrupted cache
        # WHEN inserting an item
        corrupted_cache.insert('test', 1)

        # THEN the cache should be recreated with this item
        assert len(corrupted_cache.db) == 1
        assert int(corrupted_cache.db.all()[0]['value']) == 1

    def test_remove(self, corrupted_cache):
        # GIVEN a corrupted cache
        # WHEN removing an item
        corrupted_cache.remove('test')

        # THEN empty cache should be created
        assert len(corrupted_cache.db) == 0

    def test_remove_oldest(self, corrupted_cache):
        # GIVEN a corrupted cache
        # WHEN removing an item
        corrupted_cache._remove_oldest()

        # THEN empty cache should be created
        assert len(corrupted_cache.db) == 0

    def test_remove_expired(self, corrupted_cache):
        # GIVEN a corrupted cache with timeout
        corrupted_cache.timeout = 1

        # WHEN removing expired items
        corrupted_cache._remove_expired()

        # THEN an empty cache should be created
        assert len(corrupted_cache.db) == 0

    def test_recreate_db(self, corrupted_cache_file):
        # GIVEN a corrupted database
        # WHEN initializing cache
        cache = cachalot.Cache(path=corrupted_cache_file)

        # THEN it should correctly initialize
        assert len(cache.db) == 0
